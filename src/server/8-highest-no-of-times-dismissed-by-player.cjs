// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathDeliveries = 'src/data/deliveries.csv';

fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data1) => {
    if (err) {
        console.error('Error reading CSV file:', err);
        return;
    }

    // Parse the CSV data 
    const parsedDataDeliveries = Papa.parse(data1, {
        header: true, // Treat the first row as header
    });

    let deliveriesData = parsedDataDeliveries.data;

    function findHighestDismissals(deliveriesData) {
        const dismissalCount = {};

        for (let delivery of deliveriesData) {
            const batsman = delivery['player_dismissed'];
            const bowler = delivery['bowler'];
            const dismissalKind = delivery['dismissal_kind'];

            if (dismissalKind !== "run out" && batsman !== "") {
                if (!dismissalCount[batsman]) {
                    dismissalCount[batsman] = {};
                }

                if (!dismissalCount[batsman][bowler]) {
                    dismissalCount[batsman][bowler] = 1;
                } else {
                    dismissalCount[batsman][bowler]++;
                }
            }
        }

        const highestDismissals = {};

        for (const batsman in dismissalCount) {
            let maxBowler = '';
            let maxCount = 0;

            for (const bowler in dismissalCount[batsman]) {
                const count = dismissalCount[batsman][bowler];

                if (count > maxCount) {
                    maxBowler = bowler;
                    maxCount = count;
                }
            }

            highestDismissals[batsman] = { "Bowler": maxBowler, "Dismissal Count": maxCount };
        }

        const sortedDismissals = [];

        for (const batsman in highestDismissals) {
            sortedDismissals.push([batsman, highestDismissals[batsman]]);
        }

        sortedDismissals.sort((bowler1, bowler2) => { bowler2[1]["Dismissal Count"] - bowler1[1]["Dismissal Count"] });
        return sortedDismissals[0];
    }
    
    //console.log(findHighestDismissals(deliveriesData));
    const result = findHighestDismissals(deliveriesData);
    const jsonData = JSON.stringify(result);

    fs.writeFile('src/public/output/8-highest-no-of-times-dismissed-by-player.json', jsonData, (err) => {
        if (err) {
            console.error('Error writing JSON data to the file:', err)
        }
        console.log('CSV data converted to JSON and saved as output.json');
    })
});
