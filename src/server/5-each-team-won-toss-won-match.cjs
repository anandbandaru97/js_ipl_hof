// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = 'src/data/matches.csv';
fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, //Treat the first row as header
    });

    const matchesData = parsedData.data;

    const getTeamsWonTossAndMatch = (matchesData) => {
        let teamsWonTossAndMatch = {};
        for (let match of matchesData) {
            if (!teamsWonTossAndMatch.hasOwnProperty(match['team1'])) {
                teamsWonTossAndMatch[match['team1']] = 0;
            } else if (match['toss_winner'] === match['winner']) {
                teamsWonTossAndMatch[match['winner']] += 1;
            }
        }
        return teamsWonTossAndMatch;
    }

    let result = getTeamsWonTossAndMatch(matchesData);
    const jsonData = JSON.stringify(result);

    fs.writeFile('src/public/output/5-each-team-won-toss-won-match.json', jsonData, 'utf-8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    });
});
