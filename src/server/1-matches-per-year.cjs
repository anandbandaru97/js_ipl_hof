// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = 'src/data/matches.csv';
fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, //Treat the first row as header
    });

    const matchesOfAllYears = parsedData.data;

    const getMatchesOfAllYears = (matchesOfAllYears) => {
        const allMatches = matchesOfAllYears.reduce((accumulator, match) => {
            let season = match['season'];
            if (accumulator[season] === undefined && season != null) {
                accumulator[season] = 1;
                return accumulator;
            }
            accumulator[season] += 1;
            return accumulator;
        }, {})
        return allMatches;
    }
    //console.log(getMatchesOfAllYears(matchesOfAllYears));

    let result = getMatchesOfAllYears(matchesOfAllYears);
    const jsonData = JSON.stringify(result);

    fs.writeFile('src/public/output/1-matches-per-year.json', jsonData, 'utf-8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    });
});