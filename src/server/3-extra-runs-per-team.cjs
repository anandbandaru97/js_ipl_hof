// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathDeliveries = 'src/data/deliveries.csv';
const csvFilePathMatches = 'src/data/matches.csv';

fs.readFile(csvFilePathMatches, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading CSV file:', err);
        return;
    }

    // Parse the CSV data 
    const parsedDataMatches = Papa.parse(data, {
        header: true, // Treat the first row as header
    });

    fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data1) => {
        if (err) {
            console.error('Error reading CSV file:', err);
            return;
        }

        // Parse the CSV data 
        const parsedDataDeliveries = Papa.parse(data1, {
            header: true, // Treat the first row as header
        });

        let matchesData = parsedDataMatches.data;
        let deliveriesData = parsedDataDeliveries.data;

        const extraRunsPerTeamYear = (matchesData, deliveriesData, year) => {
            const yearList = matchesData.filter((match) => { match['season'] === year });
            const idList = yearList.reduce((accumulator, year) => {
                accumulator.add(year['id']);
                return accumulator;
            }, new Set());

            const extraRunsInYear = deliveriesData.reduce((accumulator, delivery) => {
                let matchId = delivery['match_id'];
                let bowlingTeam = delivery['bowling_team'];
                let extraRuns = parseInt(delivery['extra_runs']);
                if (idList.has(matchId) && accumulator[bowlingTeam] === undefined) {
                    accumulator[bowlingTeam] = extraRuns;
                } else if (idList.has(matchId)) {
                    accumulator[bowlingTeam] += extraRuns;
                }
                return accumulator;
            }, {})
            return extraRunsInYear;
        }
        //console.log(extraRunsPerTeamYear(matchesData, deliveriesData, year));

        const result = extraRunsPerTeamYear(matchesData, deliveriesData, year = "2016");
        const jsonData = JSON.stringify(result);

        fs.writeFile('src/public/output/3-extra-runs-per-team.json', jsonData, 'utf-8', (err) => {
            if (err) {
                console.error('Error writing JSON data to file:', err);
            }
            console.log('CSV converted to JSON and saved as output.json');
        })

    });
});
